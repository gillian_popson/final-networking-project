#include <tlocCore/tloc_core.h>
#include <tlocGraphics/tloc_graphics.h>
#include <tlocMath/tloc_math.h>
#include <tlocPrefab/tloc_prefab.h>
#include <tlocApplication/tloc_application.h>

#include <gameAssetsPath.h>

#include "../../common/src/Client.h"
#include "../../common/src/Packet.h"
#include "../../common/src/PlayerInfo.h"
#include "../../common/src/GamePacketHandler.h"

#include "RakPeerInterface.h"
#include "PacketPriority.h"

TLOC_DEFINE_THIS_FILE_NAME();

using namespace tloc;

namespace {

  core_str::String shaderPathVS("/shaders/tlocOneTextureVS.glsl");
  core_str::String shaderPathFS("/shaders/tlocOneTextureFS.glsl");
  core_str::String shaderPathOneColorFS("/shaders/tlocOneColorFS.glsl");

  const tl_int g_windowWidth = 800;
  const tl_int g_windowHeight = 800;

  const core_str::String g_assetsPath(GetAssetsPath());

  math_t::Vec3f32 MouseToScreen(MousePos a_state)
  {
    using math_utils::scale_f32_f32;
    typedef math_utils::scale_f32_f32::range_small range_small;
    typedef math_utils::scale_f32_f32::range_large range_large;
    using namespace core::component_system;

    f32 maxHeightInput = (f32) g_windowHeight;
    f32 maxWidthInput = (f32) g_windowWidth;

    range_small smallR(-1.0f, 1.1f);
    range_large largeRX(0.0f, maxWidthInput);
    range_large largeRY(0.0f, maxHeightInput);
    scale_f32_f32 scx(smallR, largeRX);
    scale_f32_f32 scy(smallR, largeRY);

    return math_t::Vec3f32(scx.ScaleDown((f32)a_state.m_x), 
                           scy.ScaleDown(maxHeightInput - (f32)a_state.m_y - 1), 
                           0.0f);
  }
};

// ///////////////////////////////////////////////////////////////////////
// Server Packet Handler

class PacketHandlerClient
  : public GamePacketHandler_I
{
public:
  PacketHandlerClient(rak_peer_ptr a_peer)
    : GamePacketHandler_I(a_peer)
  { }

  bool
    HandleMessage(packet_id_type a_id, rak_peer_ptr a_peer, packet_ptr a_packet)
  {
    switch(a_id)
    {
        // Handle client packets
      case ID_ALREADY_CONNECTED:
      { 
        TLOC_LOG_DEFAULT_INFO_FILENAME_ONLY() << 
          core_str::Format("Already connected with guid %s", a_packet->guid); 
        return true;
      }
      case ID_CONNECTION_BANNED:
      { 
        TLOC_LOG_DEFAULT_INFO_FILENAME_ONLY() << "We are banned from this server";
        return true;
      }
      case ID_CONNECTION_ATTEMPT_FAILED:
      { 
        TLOC_LOG_DEFAULT_INFO_FILENAME_ONLY() << "Connection to server failed";
        return true;
      }
      case ID_INVALID_PASSWORD:
      { 
        TLOC_LOG_DEFAULT_INFO_FILENAME_ONLY() << "Password INVALID";
        return true;
      }
      case ID_CONNECTION_REQUEST_ACCEPTED:
      {

        TLOC_LOG_DEFAULT_INFO_FILENAME_ONLY() << 
          core_str::Format("Connection require ACCEPTED to %s with GUID %s",
               a_packet->systemAddress.ToString(true), a_packet->guid.ToString());
        TLOC_LOG_DEFAULT_INFO_FILENAME_ONLY() << 
          core_str::Format("My external address is %s",
               a_peer->GetExternalID(a_packet->systemAddress).ToString(true));

        return true;
      }
	  
	  case packet_id::ID_NEW_PLAYER:
	  {
		MousePos mp = *(MousePos*)a_packet->data;

		TLOC_LOG_DEFAULT_WARN_NO_FILENAME() << "New Client GUID: " << mp.m_guid.ToString();
		TLOC_LOG_DEFAULT_WARN_NO_FILENAME() << "Current Client GUID: " << GetPeer()->GetMyGUID().ToString();

		if (mp.m_guid == GetPeer()->GetMyGUID()) { return true; }

		auto itr = core::find_if(m_playerInfo.begin(), m_playerInfo.end(), [mp](const PlayerInfo& a_info)
		{
			if (a_info.m_guid == mp.m_guid) { return true; }
			return false;
		});

		if (itr == m_playerInfo.end())
		{
			TLOC_LOG_DEFAULT_WARN_NO_FILENAME() << "New player added to CLIENT: " << a_packet->guid.ToString();

			m_playerInfo.push_back(PlayerInfo().SetState(mp).SetGuid(a_packet->guid));
		}
		break;
	  }

      case packet_id::ID_PLAYER_STATE:
      {
        auto itr = core::find_if(begin_playerInfo(), end_playerInfo(), 
                                 [a_packet](const PlayerInfo& a_pi)
        {
          if (a_packet->guid == a_pi.m_guid) { return true; }
          return false;
        });

        if (itr == end_playerInfo()) 
        {
		  TLOC_LOG_DEFAULT_WARN_NO_FILENAME() << "Player was not found: " << a_packet->guid.ToString();
          m_playerInfo.push_back(PlayerInfo().SetGuid(a_packet->guid));
          itr = end_playerInfo() - 1;
        }

        MousePos mp = *(MousePos*)a_packet->data;
        itr->SetState(mp);

        return true;
      };
    }

    return false;
  }
};

#include <tlocCore/smart_ptr/tloc_smart_ptr.inl.h>
TLOC_EXPLICITLY_INSTANTIATE_ALL_SMART_PTRS(PacketHandlerClient);

// ///////////////////////////////////////////////////////////////////////
// Demo app

class Demo 
  : public Application
{
public:
  Demo()
    : Application("2LoC Engine")
    , m_nextColor(0)
  { 
    m_colors.push_back(gfx_t::Color::COLOR_BLUE);
    m_colors.push_back(gfx_t::Color::COLOR_CYAN);
    m_colors.push_back(gfx_t::Color::COLOR_GREEN);
    m_colors.push_back(gfx_t::Color::COLOR_GREY);
    m_colors.push_back(gfx_t::Color::COLOR_MAGENTA);
    m_colors.push_back(gfx_t::Color::COLOR_WHITE);
    m_colors.push_back(gfx_t::Color::COLOR_YELLOW);
    m_colors.push_back(gfx_t::Color::COLOR_BLACK);
  }

private:
  error_type Post_Initialize() override
  {
    auto& scene = GetScene();
    scene->AddSystem<gfx_cs::MaterialSystem>();
    auto meshSys = scene->AddSystem<gfx_cs::MeshRenderSystem>();
    meshSys->SetRenderer(GetRenderer());

    m_myRect = CreateRect(core_sptr::ToVirtualPtr(scene), gfx_t::Color::COLOR_RED);

    // -----------------------------------------------------------------------
    // networking

    Client::port_type port = 30001;
    bool connected = false;
    const int numTries = 10;
    for (int i = 0; i < numTries; ++i)
    {
      m_client = core_sptr::MakeUnique<Client>(port);
      if (m_client->Connect("localhost", 30000) == ErrorSuccess)
      {
        connected = true; 
        break;
      }
      ++port;
    }

    TLOC_LOG_DEFAULT_ERR_FILENAME_ONLY_IF(connected == false)
      << "Tried to connect " << numTries << " times but failed";

    if (connected)
    {
      m_ph = core_sptr::MakeUnique<PacketHandlerClient>(m_client->GetPeer());
    }

    return Application::Post_Initialize();
  }

  core_cs::entity_vptr
    CreateRect(core_cs::ecs_vptr a_scene, gfx_t::Color a_color)
  {
    //------------------------------------------------------------------------

    math_t::Rectf_c rect(math_t::Rectf_c::width(0.1f), math_t::Rectf_c::height(0.1f));

    gfx_gl::uniform_vso u_col;
    u_col->SetName("u_col").SetValueAs(a_color.GetAs<gfx_t::p_color::format::RGBA, math_t::Vec4f32>());

    auto q = a_scene->CreatePrefab<pref_gfx::QuadNoTexCoords>()
      .Dimensions(rect).Create();

    a_scene->CreatePrefab<pref_gfx::Material>()
      .AssetsPath(GetAssetsPath())
      .AddUniform(u_col.get())
      .Add(q, core_io::Path(shaderPathVS), core_io::Path(shaderPathOneColorFS));

    return q;
  }

  void
    Pre_Update(sec_type a_deltaT)
  {
    if (m_ph) { m_ph->Update(a_deltaT); }

    // send our packet
    {
      auto x = GetMouse()->GetState().m_X.m_abs;
      auto y = GetMouse()->GetState().m_Y.m_abs;
      MousePos mp; 
      mp.m_packetID = packet_id::ID_PLAYER_STATE;
      mp.m_x = x; mp.m_y = y;
      mp.m_guid = m_client->GetPeer()->GetMyGUID();
      m_client->GetPeer()->Send((const char*)&mp, sizeof(MousePos), 
                                HIGH_PRIORITY, UNRELIABLE_SEQUENCED, 0, 
                                RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);

      auto pos = MouseToScreen(mp);
      m_myRect->GetComponent<math_cs::Transform>()->SetPosition(pos);
    }

    while (m_ph->size_playerInfo() > m_otherRects.size())
    {
      m_otherRects.push_back(CreateRect(core_sptr::ToVirtualPtr(GetScene()),
        this->m_colors[m_nextColor]));
      this->m_nextColor++;
      if (this->m_nextColor >= this->m_colors.size())
      { this->m_nextColor = 0; }
    }

    core_cs::entity_ptr_array::size_type counter = 0;
    core::for_each(m_ph->begin_playerInfo(), m_ph->end_playerInfo(),
                   [&counter, this] (const PlayerInfo& a_pi)
    {
      TLOC_LOG_DEFAULT_INFO_NO_FILENAME()
        << "Player" << counter << ": " << ": "
        << a_pi.m_state.m_x << ", " << a_pi.m_state.m_y;

      auto pos = MouseToScreen(a_pi.m_state);
      m_otherRects[counter]->GetComponent<math_cs::Transform>()->SetPosition(pos);

      ++counter;
    });
  }

private:
  client_uptr m_client;
  game_packet_handler_uptr m_ph;

  core_cs::entity_vptr              m_myRect;
  core_cs::entity_ptr_array         m_otherRects;
  
  core_conts::Array<gfx_t::Color>             m_colors;
  core_conts::Array<gfx_t::Color>::size_type  m_nextColor;

};

// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

int TLOC_MAIN(int , char *[])
{
  Demo demo;
  demo.Initialize(core_ds::MakeTuple(g_windowWidth, g_windowHeight));
  demo.Run();

  //------------------------------------------------------------------------
  // Exiting
  TLOC_LOG_CORE_INFO() << "Exiting normally";

  return 0;

}
