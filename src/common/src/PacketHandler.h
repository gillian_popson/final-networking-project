#pragma once

#include <tlocCore/smart_ptr/tloc_smart_ptr.h>
#include "Packet.h"

namespace RakNet
{
  class   RakPeerInterface;
  struct  Packet;
};

class PacketHandler_I
{
public:
  typedef PacketHandler_I                   this_type;
  typedef RakNet::RakPeerInterface*         rak_peer_ptr;
  typedef RakNet::Packet*                   packet_ptr;
  typedef unsigned char                     packet_id_type;
  typedef double                            sec_type;

  PacketHandler_I(rak_peer_ptr a_peer);

  void Update(sec_type a_deltaT);

protected:
  void HandlePackets(sec_type a_deltaT, rak_peer_ptr a_peer);

  virtual void Pre_HandlePackets(sec_type a_deltaT, rak_peer_ptr a_peer);
  virtual bool HandleMessage(packet_id_type a_id, rak_peer_ptr a_peer, packet_ptr a_packet) = 0;
  virtual void Post_HandlePackets(sec_type a_deltaT, rak_peer_ptr a_peer);

  virtual void DoHandlePackets(sec_type a_deltaT, rak_peer_ptr a_peer);

private:
  rak_peer_ptr  m_peer;

public:
  TLOC_DECL_AND_DEF_GETTER_AUTO(GetPeer, m_peer);
};

TLOC_TYPEDEF_ALL_SMART_PTRS(PacketHandler_I, packet_handler);