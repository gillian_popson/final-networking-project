#include "GamePacketHandler.h"

GamePacketHandler_I::
GamePacketHandler_I(rak_peer_ptr a_peer)
: PacketHandler_I(a_peer)
{ }

#include <tlocCore/smart_ptr/tloc_smart_ptr.inl.h>
TLOC_EXPLICITLY_INSTANTIATE_ALL_SMART_PTRS(GamePacketHandler_I);