#include "Client.h"
#include "RakPeerInterface.h"

TLOC_DEFINE_THIS_FILE_NAME();

Client::
Client(port_type a_port)
: m_port(a_port)
{
  m_peer = RakNet::RakPeerInterface::GetInstance();
}

Client::
~Client()
{
  m_peer->Shutdown(300);
  RakNet::RakPeerInterface::DestroyInstance(m_peer);
}

auto
Client::
Connect(tloc::BufferArg a_serverAddress, 
        port_type a_serverPort) const -> error_type
{
  // IPV4 socket
  RakNet::SocketDescriptor sd(m_port, 0);
  sd.socketFamily = AF_INET;

  m_peer->Startup(8, &sd, 1);
  m_peer->SetOccasionalPing(true);

  if (m_peer->Connect(a_serverAddress, a_serverPort, 0, 0) !=
      RakNet::CONNECTION_ATTEMPT_STARTED)
  {
    printf("Attempt to connect to server FAILED");
    return ErrorFailure;
  }

  TLOC_LOG_DEFAULT_INFO_FILENAME_ONLY() << "\nSERVER IP addresses:";
  for (unsigned int i = 0; i < m_peer->GetNumberOfAddresses(); i++)
  {
    TLOC_LOG_DEFAULT_INFO_FILENAME_ONLY() <<
      tl_core_str::Format("%i. %s\n", i + 1, m_peer->GetLocalIP(i));
  }

  return ErrorSuccess;
}

#include <tlocCore/tloc_core.inl.h>
TLOC_EXPLICITLY_INSTANTIATE_ALL_SMART_PTRS(Client);