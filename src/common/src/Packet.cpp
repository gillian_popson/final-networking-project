#include "Packet.h"

#include "RakNetTypes.h"

namespace f_packet
{
  unsigned char GetPacketID(RakNet::Packet* p)
  {
    // we want the first byte
    return p->data[0];
  }
};