#pragma once
#include <tlocCore/utilities/tlocUtils.h>
#include "Packet.h"
#include "RakNetTypes.h"

struct PlayerInfo
{
  typedef PlayerInfo this_type;

  RakNet::RakNetGUID m_guid;
  MousePos m_state;

  TLOC_DECL_AND_DEF_SETTER_CHAIN_AUTO(SetGuid, m_guid);
  TLOC_DECL_AND_DEF_SETTER_CHAIN_AUTO(SetState, m_state);
};
