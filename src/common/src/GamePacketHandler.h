#pragma once

#include <tlocCore/containers/tloc_containers.h>
#include <tlocCore/utilities/tlocContainerUtils.h>

#include "PacketHandler.h"
#include "PlayerInfo.h"

class GamePacketHandler_I
  : public PacketHandler_I
{
public:
  typedef tl_core_conts::Array<PlayerInfo>           player_info_cont;

public:
  GamePacketHandler_I(rak_peer_ptr a_peer); 

protected:
  player_info_cont  m_playerInfo;

public:
  TLOC_DECL_AND_DEF_CONTAINER_ALL_METHODS(_playerInfo, m_playerInfo);
};

#include <tlocCore/smart_ptr/tloc_smart_ptr.h>
TLOC_TYPEDEF_ALL_SMART_PTRS(GamePacketHandler_I, game_packet_handler);