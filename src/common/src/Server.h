#pragma once

#include <tlocCore/tloc_core.h>
#include "PacketHandler.h"

namespace RakNet { class RakPeerInterface; };

class Server
{
public:
  typedef unsigned short            port_type;
  typedef tl_core::error::Error     error_type;
  typedef RakNet::RakPeerInterface* rak_peer_interface_ptr;

public:
  Server(port_type a_port);
  ~Server();
  error_type Launch() const;

private:
  port_type               m_port;
  rak_peer_interface_ptr  m_peer;


public:
  TLOC_DECL_AND_DEF_GETTER_AUTO(GetPort, m_port);
  TLOC_DECL_AND_DEF_GETTER_AUTO(GetPeer, m_peer);
};

TLOC_TYPEDEF_ALL_SMART_PTRS(Server, server);