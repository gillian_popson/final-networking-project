#pragma once

#include <tlocCore/tloc_core.h>

namespace RakNet { class RakPeerInterface; };

class Client
{
public:
  typedef unsigned short            port_type;
  typedef tl_core::error::Error     error_type;
  typedef RakNet::RakPeerInterface* rak_peer_interface_ptr;

public:
  Client(port_type a_port);
  ~Client();
  error_type Connect(tloc::BufferArg a_serverAddress, 
                     port_type a_serverPort) const;

private:
  port_type               m_port;
  rak_peer_interface_ptr  m_peer;


public:
  TLOC_DECL_AND_DEF_GETTER_AUTO(GetPort, m_port);
  TLOC_DECL_AND_DEF_GETTER_AUTO(GetPeer, m_peer);
};

TLOC_TYPEDEF_ALL_SMART_PTRS(Client, client);