#pragma once

#include "MessageIdentifiers.h"
#include "RakNetTypes.h"

namespace RakNet{ struct Packet; };

namespace packet_id
{
  enum {
	  ID_PLAYER_STATE = ID_USER_PACKET_ENUM,
	  ID_NEW_PLAYER
  };
};

#pragma pack(push, 1)
struct MousePos
{
public:
  typedef unsigned char        packet_id_type;

  packet_id_type m_packetID;
  int m_x, m_y;
  RakNet::RakNetGUID m_guid;
};
#pragma pack(pop)

namespace f_packet
{
  unsigned char GetPacketID(RakNet::Packet* p);
};