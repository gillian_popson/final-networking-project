#------------------------------------------------------------------------------
# This file is included AFTER CMake adds the executable/library
# Do NOT remove the following variables. Modify the variables to suit your 
# project.

# Do NOT remove the following variables. Modify the variables to suit your project
set(SOLUTION_SOURCE_FILES
  src/Server.h
  src/Server.cpp
  src/Client.h
  src/Client.cpp
  src/GamePacketHandler.h
  src/GamePacketHandler.cpp
  src/Packet.h
  src/Packet.cpp
  src/PacketHandler.h
  src/PacketHandler.cpp
  src/PlayerInfo.h
  )

# Do not include individual assets here. Only add paths
set(SOLUTION_ASSETS_PATH
  ../../assets
  )

# Dependent project is compiled after dependency
set(SOLUTION_PROJECT_DEPENDENCIES
  )

# Libraries that the executable needs to link against
set(SOLUTION_EXECUTABLE_LINK_LIBRARIES
  )
